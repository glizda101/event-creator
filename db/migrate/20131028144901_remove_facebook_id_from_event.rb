class RemoveFacebookIdFromEvent < ActiveRecord::Migration
  def change
    remove_column :events, :facebook_id, :integer
  end
end

class AddFacebookIntegerIdToEvent < ActiveRecord::Migration
  def change
    add_column :events, :facebook_id, :integer
  end
end
